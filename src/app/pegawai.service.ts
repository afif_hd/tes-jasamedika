import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class PegawaiService {
  private apiUrl: string = "http://api.jasamedika.co.id/service/pegawai";
  private apiJabatan: string = "http://api.jasamedika.co.id/service/jabatan";
  private apiDokumen: string = "http://api.jasamedika.co.id/public/service/dokumen";

  constructor(private http:Http) { }

  getPegawai(){
		return this.http.get(this.apiUrl).map((response:Response)=>response.json().data);
  }
  postPegawai(data){
    console.log(data);
    return this.http.post(this.apiUrl,data).map((response: Response)=>response.json());
  }
  updatePegawai(data){
    console.log(this.apiUrl+'/'+data.ID);
    return this.http.put(this.apiUrl+'/'+data.ID,data).map((response:Response)=>response.json());
  }
  deletePegawai(data){
    return this.http.delete(this.apiUrl+'/'+data.ID).map((response:Response)=>response.json());
  }  

  getJabatan(){
		return this.http.get(this.apiJabatan).map((response:Response)=>response.json().data);
  }
  getDokumen(){
		return this.http.get(this.apiDokumen).map((response:Response)=>response.json().data);
	}

}
