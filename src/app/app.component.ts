import { Component } from '@angular/core';
import { PegawaiService } from "./pegawai.service";
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private pegSvc:PegawaiService,){}

  pegawai;
  jabatan;
  dokumen;
  peg_model={};

  newPegawai=false;
  editpegawai=false;

  ngOnInit(){

    this.pegSvc.getPegawai().subscribe(res=>
      {
        this.pegawai = res
        console.log(this.pegawai)
      }
    );
    this.pegSvc.getJabatan().subscribe(res=>
      {
        this.jabatan = res
        console.log(this.jabatan)
      }
    );
    this.pegSvc.getDokumen().subscribe(res=>
      {
        this.dokumen = res
        console.log(this.dokumen)
      }
    );
  }

  tambah(){
    this.peg_model = {};
    this.newPegawai=true;
    this.editpegawai=false;
  }

  addPegawai(data){
    let listDok = []
    data.documents.map(x=>{
      listDok.push({ID:data.ID,ID_Klp_Dok:x})})
    data.documents = listDok
    console.log("tess",data);
    this.pegSvc.postPegawai(data).subscribe(res=>{console.log(res);this.refreshData()})

  }
  
  editPegawai(data){
    console.log("edit tess",data);
    this.peg_model = data;
    this.newPegawai=false;
    this.editpegawai=true;
  }

  putPegawai(data){
    console.log(data);
    this.pegSvc.updatePegawai(data).subscribe(res=>{console.log(res);this.refreshData()})
  }

  delPegawai(data){
    console.log("del tess",data);
    if (confirm("Delete Pegawai : ("+data.ID+") - "+data.Nama)){
      this.pegSvc.deletePegawai(data).subscribe(res=>{
        console.log(res);
        console.log("data dihapus");
        this.refreshData();
      })
      
    }
  }

  refreshData(){
    this.newPegawai=false;
    this.editpegawai=false;
    this.pegawai=[];
    this.pegSvc.getPegawai().subscribe(res=>
      {
        this.pegawai = res
        console.log(this.pegawai)
      }
    )
  }
}

