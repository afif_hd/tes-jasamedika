import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap';



import { AppComponent } from './app.component';

import { PegawaiService } from "./pegawai.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule
  ],
  providers: [
    PegawaiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
